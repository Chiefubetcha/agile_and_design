package com.ait.test;
//Unit test
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.Assert.assertThat;

import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import com.ait.game.Die;
import com.ait.game.PairOfDice;


class DieTest {

	//@Test 1 - Value greater than 0, and less than 7
	@RepeatedTest(20)
	void testDieReturnsValueBetweenLimits() {
		Die die=new Die();
		die.roll();
		int value=die.getValue();
		assertThat(value, allOf(greaterThan(0), lessThan(7)));
	}

	//@Test 2 - Pair Of Dice Sum
	@RepeatedTest(20)
	void testPairOfDiceReturnCorrectSum() {
		PairOfDice pod=new PairOfDice();
		pod.roll();
		int v1 = pod.getValue1();
		int v2 = pod.getValue2();
		assertThat(v1, allOf(greaterThan(0), lessThan(7)));
		assertThat(v2, allOf(greaterThan(0), lessThan(7)));
	}
}
