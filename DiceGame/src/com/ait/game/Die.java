package com.ait.game;
public class Die
{
    private int value;
    private static int SIDES=6;

    public Die()
    {
       this.value = 1;  // initialize value to 1
    }


    // roll the die
    public void roll()
    {
        value = (int) (Math.random() * SIDES + 1);  // number from 0 to sides
    }

    public int getValue()
    {
        return value;
    }
}